#!/bin/sh

sed -i "s|SCRAPE_INTERVAL|${SCRAPE_INTERVAL}|g" /etc/prometheus/prometheus.yml
sed -i "s|EVALUATION_INTERVAL|${EVALUATION_INTERVAL}|g" /etc/prometheus/prometheus.yml
sed -i "s|INTEGRATION_SERVICE_ADDRESS|${INTEGRATION_SERVICE_ADDRESS}|g" /etc/prometheus/prometheus.yml
sed -i "s|METRICS_PATH|${METRICS_PATH}|g" /etc/prometheus/prometheus.yml

prometheus --config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/prometheus --web.console.libraries=/usr/share/prometheus/console_libraries --web.console.templates=/usr/share/prometheus/consoles --web.route-prefix="${ROUTE_PREFIX}" --web.external-url="${EXTERNAL_URL}"
