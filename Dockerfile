FROM prom/prometheus

ADD ./config/prometheus.yml /etc/prometheus

COPY ./custom-entrypoint.sh /custom-entrypoint.sh

EXPOSE 9090

ENTRYPOINT ["sh", "/custom-entrypoint.sh"]